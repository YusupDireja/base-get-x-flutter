import 'package:base_getx/model/dummy/dummy_response.dart';
import 'package:get/get.dart';

class DummyRepository extends GetConnect {
  Future<List<DummyResponse>> getPost() async {
    List<DummyResponse> response = [];

    Response raw = await get("https://jsonplaceholder.typicode.com/posts",
        contentType: 'application/json');
    var rawData = raw.body;

    if (raw.isOk) {
      response = List<DummyResponse>.from(
          rawData.map((data) => DummyResponse.fromJson(data)));
    }

    return response;
  }
}
