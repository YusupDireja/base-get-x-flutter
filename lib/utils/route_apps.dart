import 'package:base_getx/pages/auth/login_page.dart';
import 'package:base_getx/pages/index/index_page.dart';
import 'package:base_getx/pages/splash/splash_page.dart';
import 'package:base_getx/utils/constants/navigation.dart';
import 'package:get/get_navigation/src/routes/get_route.dart';

List<GetPage> routeApps() {
  var routes = <GetPage>[];
  routes.add(
      GetPage(name: Navigation.splashPage, page: () => const SplashPage()));
  routes
      .add(GetPage(name: Navigation.loginPage, page: () => const LoginPage()));
  routes
      .add(GetPage(name: Navigation.indexPage, page: () => const IndexPage()));
  return routes;
}
