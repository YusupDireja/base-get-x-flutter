import 'package:get/get.dart';

class Dictionary extends Translations {
  static const appName = "Base Project GetX";

  static const hello = "hello";
  static const logout = "logout";
  static const title = "title";
  static const description = "description";

  @override
  Map<String, Map<String, String>> get keys => {
        'en_US': {
          hello: 'Hello World',
          logout: 'Logout',
          title: 'Title',
          description:'Description',
        },
        'id_ID': {
          hello: 'Halo Dunia',
          logout: 'Keluar',
          title: 'Judul',
          description: 'Deskripsi',
        },
      };
}
