class Endpoint {
  static const baseUrlDev = "https://dev.base-project-getx/v1";
  static const publicUrlDev = "https://dev.base-project-getx/public/";

  static const baseUrlProd = "https://base-project-getx/v1";
  static const publicUrlProd = "https://base-project-getx/public/";
}