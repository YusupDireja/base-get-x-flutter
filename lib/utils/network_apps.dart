import 'package:base_getx/utils/config_apps.dart';

class NetworkApps {
  static String baseUrl = ConfigApps.instance.baseUrl;
  static String publicUrl = ConfigApps.instance.publicUrl;
}