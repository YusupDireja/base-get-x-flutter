import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class ThemeApps {
  static final ThemeData lightTheme = ThemeData.light().copyWith(
    appBarTheme: const AppBarTheme().copyWith(
      systemOverlayStyle: const SystemUiOverlayStyle().copyWith(
        statusBarColor:  Colors.orange,
      )
    ),
    colorScheme: ColorScheme.fromSwatch().copyWith(
        primary: Colors.orange, secondary: Colors.amber),
    scaffoldBackgroundColor: Colors.grey[200],
  );

  static final ThemeData darkTheme = ThemeData.dark().copyWith(
    colorScheme: ColorScheme.fromSwatch().copyWith(
        primary: Colors.orange, secondary: Colors.amber),
  );
}
