import 'dart:convert';

import 'package:base_getx/model/auth_model.dart';
import 'package:get_storage/get_storage.dart';

class StorageApps {
  static const boxAuth = "BOX_AUTH";

  static writeAuth(AuthModel auth) async {
    final box = GetStorage();
    await box.write(boxAuth, jsonEncode(auth));
  }

  static AuthModel? readAuth() {
    final box = GetStorage();
    var auth = box.read(boxAuth);
    return auth != null ? AuthModel.fromJson(jsonDecode(auth)) : null;
  }

  static removeAuth() async {
    final box = GetStorage();
    await box.remove(boxAuth);
  }
}
