import 'package:flutter/foundation.dart';

enum Flavor { development, production }

/**
 * Membuat Singletone Config Apps
 * - Dimana dapat memangil instance yang sama (tidak membuat instance baru)
 */
class ConfigApps {
  final Flavor flavor;
  final String baseUrl;
  final String publicUrl;
  static ConfigApps? _instance;

  factory ConfigApps(
      {required Flavor flavor,
        required String baseUrl,
        required String publicUrl}) {

    _instance ??= ConfigApps._create(
          flavor, baseUrl,publicUrl);
    return _instance!;
  }

  ConfigApps._create(this.flavor, this.baseUrl, this.publicUrl);

  /**
   * Getter Setter / Fungsi
   */
  static ConfigApps get instance {
    return _instance!;
  }
  static bool isProduction() => _instance!.flavor == Flavor.production;
  static bool isStaging() => _instance!.flavor == Flavor.development;
}
