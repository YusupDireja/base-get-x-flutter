import 'package:base_getx/pages/splash/splash_page.dart';
import 'package:base_getx/utils/config_apps.dart';
import 'package:base_getx/utils/constants/dictionary.dart';
import 'package:base_getx/utils/constants/endpoint.dart';
import 'package:base_getx/utils/theme_apps.dart';
import 'package:flutter/material.dart';

void main() {
  ConfigApps(
      flavor: Flavor.production,
      baseUrl: Endpoint.baseUrlProd,
      publicUrl: Endpoint.publicUrlProd);

  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: Dictionary.appName,
      theme: ThemeApps.lightTheme,
      darkTheme: ThemeApps.darkTheme,
      home: const SplashPage(),
    );
  }
}