import 'package:base_getx/utils/config_apps.dart';
import 'package:base_getx/utils/constants/dictionary.dart';
import 'package:base_getx/utils/constants/endpoint.dart';
import 'package:base_getx/utils/constants/navigation.dart';
import 'package:base_getx/utils/route_apps.dart';
import 'package:base_getx/utils/theme_apps.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';

Future<void> main() async {
  ConfigApps(
      flavor: Flavor.development,
      baseUrl: Endpoint.baseUrlDev,
      publicUrl: Endpoint.publicUrlDev);

  await GetStorage.init();

  runApp(GetMaterialApp(
    debugShowCheckedModeBanner: false,
    title: Dictionary.appName,
    initialRoute: Navigation.splashPage,
    translations: Dictionary(),
    locale: const Locale('id', 'ID'),
    fallbackLocale: const Locale('id', 'ID'),
    getPages: routeApps(),
    theme: ThemeApps.lightTheme,
    darkTheme: ThemeApps.darkTheme,
  ));
}
