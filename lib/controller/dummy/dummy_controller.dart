import 'package:base_getx/model/dummy/dummy_response.dart';
import 'package:base_getx/repository/dummy/dummy_repository.dart';
import 'package:get/get.dart';

class DummyController extends GetxController {
  var listDummy = <DummyResponse>[].obs;

  Future<void> fetchDummy() async {
    var response = await DummyRepository().getPost();
    listDummy.value = response;
  }
}
 