import 'dart:convert';
import 'dart:developer';

import 'package:base_getx/controller/dummy/dummy_controller.dart';
import 'package:base_getx/model/auth_model.dart';
import 'package:base_getx/utils/constants/dictionary.dart';
import 'package:base_getx/utils/constants/navigation.dart';
import 'package:base_getx/utils/storage_apps.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class IndexPage extends StatefulWidget {
  const IndexPage({Key? key}) : super(key: key);

  @override
  _IndexPageState createState() => _IndexPageState();
}

class _IndexPageState extends State<IndexPage> {
  DummyController controller = Get.put(DummyController());
  var isBahasa = true.obs;

  Future<void> initialPage() async {
    var auth = Get.parameters['auth'];
    if (auth != null) {
      AuthModel auth = AuthModel.fromJson(jsonDecode(Get.parameters['auth']!));
      log(">>> param auth ${auth.toString()}");
    }

    AuthModel? authStorage = StorageApps.readAuth();
    log(">>> storage auth ${authStorage.toString()}");

    await controller.fetchDummy();
    log("message ${controller.listDummy.length} ");
  }

  @override
  void initState() {
    initialPage();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text(Dictionary.appName),
        actions: [
          Switch(
            value: isBahasa.value,
            activeColor: Colors.amberAccent,
            activeTrackColor: Colors.amberAccent[200],
            onChanged: (value) {
              isBahasa.value = value;
              var locale = isBahasa.isTrue
                  ? const Locale('id', 'ID')
                  : const Locale('en', 'US');
              Get.updateLocale(locale);
            },
          ),
        ],
      ),
      body: Column(
        children: [
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(Dictionary.hello.tr),
                ElevatedButton(
                  onPressed: () {
                    logout();
                  },
                  child: Text(Dictionary.logout.tr),
                ),
              ],
            ),
          ),
          Expanded(
            child: Obx(
              () => ListView.builder(
                shrinkWrap: true,
                itemCount: controller.listDummy.length,
                padding: const EdgeInsets.all(8.0),
                physics: const BouncingScrollPhysics(),
                itemBuilder: (context, index) {
                  return Card(
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            '${Dictionary.title.tr} : ${controller.listDummy[index].title}',
                            style: const TextStyle(fontWeight: FontWeight.bold),
                          ),
                          Text(
                              '${Dictionary.description.tr} - ${controller.listDummy[index].body}'),
                        ],
                      ),
                    ),
                  );
                },
              ),
            ),
          ),
        ],
      ),
    );
  }

  void logout() {
    StorageApps.removeAuth();
    Get.offAllNamed(Navigation.loginPage);
  }
}
