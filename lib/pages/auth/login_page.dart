import 'dart:convert';
import 'dart:developer';

import 'package:base_getx/model/auth_model.dart';
import 'package:base_getx/utils/constants/navigation.dart';
import 'package:base_getx/utils/storage_apps.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({Key? key}) : super(key: key);

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {

  final txtUsername = TextEditingController();
  final txtPassword = TextEditingController();
  final formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        toolbarHeight: 0,
      ),
      body: Center(
        child: Padding(
          padding: const EdgeInsets.all(58.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              const Icon(
                Icons.star,
                size: 40,
              ),
              Form(
                key: formKey,
                child: Column(
                  children: [
                    TextFormField(
                      controller: txtUsername,
                      validator: (value) {
                        if (value == null || value.isEmpty) {
                          return 'Please enter some text';
                        }
                        return null;
                      },
                      decoration: const InputDecoration(hintText: "Username"),
                    ),
                    TextFormField(
                      controller: txtPassword,
                      validator: (value) {
                        if (value == null || value.isEmpty) {
                          return 'Please enter some text';
                        }
                        return null;
                      },
                      decoration: const InputDecoration(hintText: "Password"),
                    ),
                    ElevatedButton(
                      onPressed: () {
                        onSubmit();
                      },
                      child: const Text('Login'),
                    )
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  void onSubmit(){
    if(formKey.currentState!.validate()){

      var auth = AuthModel(txtUsername.text, txtPassword.text);

      StorageApps.writeAuth(auth);
      Get.offAndToNamed(Navigation.indexPage,parameters: {'auth': jsonEncode(auth)});
    }
  }
}