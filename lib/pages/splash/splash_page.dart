import 'dart:developer';

import 'package:base_getx/utils/constants/dictionary.dart';
import 'package:base_getx/utils/constants/navigation.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../model/auth_model.dart';
import '../../utils/storage_apps.dart';

class SplashPage extends StatefulWidget {
  const SplashPage({Key? key}) : super(key: key);

  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashPage> {
  @override
  void initState() {
    Future.delayed(
      const Duration(seconds: 5),
      () {
        var auth = StorageApps.readAuth();
        if (auth != null) {
          Get.offAndToNamed(Navigation.indexPage);
        } else {
          Get.offAndToNamed(Navigation.loginPage);
        }
      },
    );
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(toolbarHeight: 0),
        body: Center(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: const [
              Icon(
                Icons.star,
                size: 40,
              ),
              Text(Dictionary.appName)
            ],
          ),
        ));
  }
}
