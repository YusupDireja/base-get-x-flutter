class AuthModel {
  String? username;
  String? password;

  AuthModel(this.username, this.password);

  AuthModel.fromJson(Map<String, dynamic> json) {
    username = json['username'];
    password = json['password'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['username'] = username;
    data['password'] = password;
    return data;
  }

  @override
  String toString() {
    return 'AuthModel{username: $username, password: $password}';
  }
}